In order to get started you need to have `nodejs` version 6? or later installed; as well as `yarn` for your pacakge manager.

Before you are able to run any parts of this project you are required to install all the dependant packages by running:
```bash
$ yarn
```

In order to run the SPA in development mode run the following command
```bash
$ yarn start
```

If you wish to view and test individual components you can runx (take note: you must have `npx` installed as part of `npm`)

```bash
$ yarn styleguide
```
