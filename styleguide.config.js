const path = require('path');

module.exports = {
  webpackConfig: require('./config/webpack.config.js'),
  require: [
    path.join(__dirname, 'public/assets/css/core.css'),
    // path.join(__dirname, '../node_modules/fortawesome/fontawesome-pro/css/all.css'),
    path.join(__dirname, 'src/styles.scss'),
  ],
  sections: [
    {
      name: 'Introduction',
      content: 'docs/introduction.md'
    },
    {
      name: 'Documentation',
      sections: [
        {
          name: 'Getting Started',
          content: 'docs/getting-started.md',
        },
        {
          name: 'Live Demo',
          extermal: true,
          href: 'https://education.nsw.gov.au/search',
        }
      ]
    },
    {
      name: 'UI Components',
      sections: [
        {
          name: 'General Components',
          components: [
            'src/components/LoadingIndicator/*.js',
            'src/components/Card/*.js',
            'src/components/Input/*.js',
            'src/components/ShowHide/*.js',
          ]
        },
        {
          name: 'Result Types',
          content: 'docs/resultTypes.md',
          components: [
            'src/components/ResultTypes/Calendar.js',
            'src/components/ResultTypes/Doc.js',
            'src/components/ResultTypes/HtmlFragment.js',
            'src/components/ResultTypes/News.js',
            'src/components/ResultTypes/ResultTile.js',
            'src/components/ResultTypes/School.js',
            'src/components/ResultTypes/StaffOnly.js',
          ],
        }
      ]
    }
  ]
};
