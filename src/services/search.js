import queryString from 'query-string';
import { ajaxFetch, removeTrailingSlash } from '../utils/utils';
import { getSiteOptions } from '../utils/meta';

const itemsPerPage = 10;

function getItemsPerPage() {
  const { doeSearch } = window;
  const { view } = doeSearch || {};
  const { type } = view || {};

  if (type === 'catalogue') {
    return 12;
  }

  return itemsPerPage;
}

const buildPartialFields = (properties = {}) => {
  const propertiesKeys = Object.keys(properties);
  const partialFields = propertiesKeys.reduce((acc, propertyKey, index) => {
    if (propertyKey === 'as_sitesearch' || propertyKey === 'filterBy') {
      return `${acc}`;
    }

    const value = properties[propertyKey]; 

    if (typeof value === 'undefined' || value === '') {
      return acc;
    }

    const encodedKey = encodeURIComponent(propertyKey);
    const encodedValue = encodeURIComponent(properties[propertyKey]);

    const field = `${encodedKey}:${encodedValue}`;

    return index < propertiesKeys.length - 1
      ? `${acc}${field}.`
      : `${acc}${field}`;
  }, '');

  return partialFields;
}


const filterByMap = {
  documents: { oni_type: 'file' },
  schools: { doe_document_type: 'orgunit' },
  event: { doe_document_type: 'event' },
  article: { doe_document_type: 'article' },
  policy: { doe_document_type: 'POLICY' },
  pages: { oni_type: 'html' },
}


const generateAsSiteSearch = (path) => {
  const siteOptions = getSiteOptions();
  const { site, ignoreSiteForGlobal } = siteOptions;
  return (ignoreSiteForGlobal && !path)
    ? ''
    : `${removeTrailingSlash(site)}${path}`
}


const buildSearchUrl = (query, queryType, searchIn, filterBy = '', start, facets = {}) => {
  const { doeSearch } = window;
  const { siteUrl } = doeSearch || {};

  const siteOptions = getSiteOptions();
  
  const params = {
    q: query,
    site: siteOptions.collection,
    client: siteOptions.client,
    getfields: '*',
    format: 'json',
    num: getItemsPerPage(),
  };
  
  let as_sitesearch = undefined;

  if (!!facets.as_sitesearch) {
    as_sitesearch = `${siteUrl}${facets.as_sitesearch}`;
  }

  if (!as_sitesearch) {
    as_sitesearch = generateAsSiteSearch(
      searchIn === 'all' ? '' : searchIn || ''
    );
  }

  /*
   * Only add `as_sitesearch` property if truthy (not empty string)
   * because otherwise it slows down the response time of searches
   */
  if (!!as_sitesearch) {
    params.as_sitesearch = as_sitesearch;
  }

  const filterByObject = filterByMap[facets.filterBy] || filterByMap[filterBy] || {};
  let partialFields = buildPartialFields({ ...filterByObject, ...facets });

  if (typeof partialFields !== 'undefined' && partialFields !== '') {
    params.partialfields = partialFields;
  }

  if (start !== null) {
    params.start = start;
  }

  return `${removeTrailingSlash(siteOptions.baseUrl)}/${queryType}?${queryString.stringify(params)}`;
};


const search = (query, searchIn, filterBy, start, facets) => (
  ajaxFetch(
    buildSearchUrl(query, 'search', searchIn, filterBy, start, facets)
  )
);


const searchSuggestions = (query, searchIn, filterBy) => (
  ajaxFetch(
    buildSearchUrl(query, 'suggest', searchIn, filterBy)
  )
);


export { search, searchSuggestions };
