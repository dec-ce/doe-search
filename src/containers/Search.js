/* eslint-disable jsx-a11y/anchor-is-valid, no-script-url */

import React, { Component } from "react";
import isMobile from 'ismobilejs';
import SearchContext from '../shared/SearchContext.js';
import SiteSearchView from './SearchView/SiteSearchView';
import CatalogueSearchView from './SearchView/CatalogueSearchView';

const focusInputOnSuggestionClick = !isMobile.any;

class Search extends Component {
  static contextType = SearchContext.Context;

  execDidYouMean = async () => {
    const { didYouMean, onResetSearch, onSearchFetch, onSetQuery } = this.context;
    await onResetSearch();
    await onSetQuery(didYouMean);
    await onSearchFetch(true);
  };

  isPrivate = () => {
    const { isSecure } = this.context;

    if (typeof isSecure === 'undefined') {
      const metaElements = document.getElementsByName("security.public");
      return (
        metaElements.length > 0 &&
        metaElements[0].content.indexOf('no') >= 0
      );
    }

    return isSecure;
  }

  async componentDidMount() {
    const { doeSearch } = window;
    const {
      facets = [],
      isStaffOnly,
      searchingIn,
      lookingForSomethingElse,
      backTo,
      view = {},
    } = doeSearch || {};

    const { queryParams = {} } = this.props;
    const {
      onSetPage,
      onSetQuery,
      onSearchFetch,
      onSetFacets, 
      onSetIsSecure,
      onSetSearchInText,
      onSetLookingForSomethingElse,
      onSetBackTo,
    } = this.context;

    await onSetPage();
    await onSetQuery(queryParams.q || '');

    const facetValues = facets.reduce((acc, val) => ({ ...acc, [val.id]: queryParams[val.id] }), {});
    await onSetFacets(facetValues);

    await onSetIsSecure(isStaffOnly);
    await onSetSearchInText(searchingIn);
    await onSetLookingForSomethingElse(lookingForSomethingElse);
    await onSetBackTo(backTo);

    if (queryParams.q || view.type === 'catalogue') {
      await onSearchFetch(true);
    }
  }

  render() {
    const { doeSearch } = window;
    const {
      view = {},
    } = doeSearch || {};
    const { didYouMean, didYouMeanHtml, loading } = this.context;

    const didYouMeanElement = didYouMean ? (
      <p className="search__autosuggest__sugested-spelling mb-0">
        Did you mean: <a dangerouslySetInnerHTML={{ __html: didYouMeanHtml }} onClick={this.execDidYouMean} href="javascript:;"></a>
      </p>
    ) : null;
    
    
    const SearchViewComponent = view.type === 'catalogue'
      ? CatalogueSearchView
      : SiteSearchView;

    return (
      <SearchViewComponent 
        loading={loading}
        didYouMeanElement={didYouMeanElement}
        isPrivate={this.isPrivate()}
        focusInputOnSuggestionClick={focusInputOnSuggestionClick}
      />
    );
  }
}

export default Search;
