/* eslint-disable jsx-a11y/anchor-is-valid, no-script-url */

import React, {Fragment} from 'react';
import ReactPaginate from 'react-paginate';
import SearchContext from '../../shared/SearchContext';
import { buildResultTileFragment } from '../../utils/resultBuilder';

class SearchResults extends React.PureComponent {
  static contextType = SearchContext.Context;

  handlePageClick = async (data) => {
    await this.context.onSetPage(data.selected);
    await this.context.onSearchFetch(false, () => {
      document.getElementById("searchResults").focus();
      window.scrollTo(0, 0);
    });
  };

  getFragmentFromResult = (result) => buildResultTileFragment(result);

  processResults = (results) => results.reduce((acc, result) => {
    const fragment = this.getFragmentFromResult(result);
    return fragment ? [...acc, fragment] : [...acc]
  }, []);


  render() {
    const { result, selectedPage } = this.context;

    if (!result) { return null; }

    const { results, totalHits, size } = result;

    const resultItems = this.processResults(results || []);

    const pagination = {
      hasMorePages: totalHits > size,
      showPages: 3,
      afterEllipsisPages: 1,
      pageCount: totalHits % size === 0 ? totalHits / size : Math.ceil(totalHits / size)
    };

    return (
      <Fragment>
        <div className="gel-search-results" id="searchResults" role="region" aria-live="polite">
          <div className="row mt-4">
            { resultItems }
          </div>
        </div>

        { pagination.hasMorePages ?
          <nav>
            <ReactPaginate
              previousLabel={<span>Previous</span>}
              nextLabel={<span>Next</span>}
              breakLabel={<span>...</span>}
              breakClassName="gel-gel-pagination__more gel-pagination__disabled"
              breakLinkClassName="gef-remove-external-link gel-remove-external-link"
              pageCount={pagination.pageCount}
              marginPagesDisplayed={pagination.afterEllipsisPages}
              pageRangeDisplayed={pagination.showPages}
              onPageChange={this.handlePageClick}
              containerClassName="gel-pagination"
              activeClassName="active gel-pagination__active"
              disabledClassName="gel-pagination__disabled"
              forcePage={selectedPage}
              pageClassName=""
              pageLinkClassName="gef-remove-external-link gel-remove-external-link"
              previousClassName="gel-pagination__prev"
              nextClassName="gel-pagination__next"
              previousLinkClassName={"gef-remove-external-link gel-remove-external-link"}
              nextLinkClassName={"gef-remove-external-link gel-remove-external-link"}
            />
          </nav>
          : null
        }
      </Fragment>
    );

  }
}

export default SearchResults;

