/* eslint-disable jsx-a11y/anchor-is-valid, no-script-url */

import React, {Fragment} from 'react';
import ReactPaginate from 'react-paginate';

import { Recommended, RecommendedItem } from '../../components/Recommended';
import SearchContext from '../../shared/SearchContext';

import ResultsHeader from '../../components/ResultsHeader';
import LookElsewhere from '../../components/LookElsewhere';

import {
  buildEventFragment,
  buildMapFragment,
  buildDocFragment,
  buildSummaryHtmlFragment,
  buildNewsFragment,
} from '../../utils/resultBuilder';

class SearchResults extends React.PureComponent {
  static contextType = SearchContext.Context;

  handlePageClick = async (data) => {
    await this.context.onSetPage(data.selected);
    await this.context.onSearchFetch(false, () => {
      document.getElementById("gel-search-results-nav").focus();
      window.scrollTo(0, 0);
    });
  };

  getResultType = (metadata = {}) => {
    const {
      doe_document_type,
      document_type,
      oni_type,
      doe_orgunit_type,
      DCTERMS_identifier,
    } = metadata;

    if (doe_document_type === 'event' || doe_document_type === 'orgunit') {
      return doe_document_type;
    }

    if (document_type === 'Policy') {
      return 'doc';
    }

    if (DCTERMS_identifier && DCTERMS_identifier.match(/\/news\//i)) {
      return 'news';
    }

    if (oni_type === 'file') {
      return 'doc';
    }

    if (doe_orgunit_type === 'school') {
      return 'orgunit';
    }

    
  
    return 'default';
  };

  getFragmentFromResult = (resultType, result) => {
    switch (resultType) {
      case 'event':
        return buildEventFragment(result);
      case 'orgunit':
        return buildMapFragment(result);
      case 'doc':
        return buildDocFragment(result);
      case 'news':
        return buildNewsFragment(result);
      case 'default':
        return buildSummaryHtmlFragment(result);
      default:
        return;
    }
  };

  processResults = (results) => results.reduce((acc, result) => {
    const resultType = this.getResultType(result.metatags);
    const fragment = this.getFragmentFromResult(resultType, result);
    return fragment ? [...acc, fragment] : [...acc]
  }, []);


  render() {
    const { lastSearchQuery, result, selectedPage, lookingForSomethingElse } = this.context;

    if (!result) { return null; }

    const { keymatches, from, results, totalHits, size } = result;

    const resultsMessage = totalHits
      ? `Showing ${from + 1} - ${from + size} of ${totalHits || ''} results for`
      : `0 results for`;

    const resultItems = this.processResults(results || []);

    const pagination = {
      hasMorePages: totalHits > size,
      showPages: 3,
      afterEllipsisPages: 1,
      pageCount: totalHits % size === 0 ? totalHits / size : Math.ceil(totalHits / size)
    };

    return (
      <Fragment>
        <div className="gel-search-results" id="searchResults" role="region" aria-live="polite">
          <ResultsHeader
            message={resultsMessage}
            query={lastSearchQuery}
          />

          { keymatches && keymatches.length > 0 &&
            <Fragment>
              <Recommended>
                { keymatches.map(item => (
                  <RecommendedItem
                    title={item.title}
                    url={item.url}
                    content={item.Content}
                    key={item.title}
                  />
                ))}
              </Recommended>
            </Fragment>
          }

          <ol className="gel-search__results mt-4">
            { resultItems }

            { lookingForSomethingElse &&
              <LookElsewhere.Page
                location={lookingForSomethingElse.url}
                text={lookingForSomethingElse.label}
                showLock={lookingForSomethingElse.showLock}
                noResults={resultItems.length <= 0}
              />
            }
          </ol>
        </div>

        { pagination.hasMorePages ?
          <nav>
            <ReactPaginate
              previousLabel={<span>Previous</span>}
              nextLabel={<span>Next</span>}
              breakLabel={<span>...</span>}
              breakClassName="gel-gel-pagination__more gel-pagination__disabled"
              breakLinkClassName="gef-remove-external-link gel-remove-external-link"
              pageCount={pagination.pageCount}
              marginPagesDisplayed={pagination.afterEllipsisPages}
              pageRangeDisplayed={pagination.showPages}
              onPageChange={this.handlePageClick}
              containerClassName="gel-pagination"
              activeClassName="active gel-pagination__active"
              disabledClassName="gel-pagination__disabled"
              forcePage={selectedPage}
              pageClassName=""
              pageLinkClassName="gef-remove-external-link gel-remove-external-link"
              previousClassName="gel-pagination__prev"
              nextClassName="gel-pagination__next"
              previousLinkClassName={"gef-remove-external-link gel-remove-external-link"}
              nextLinkClassName={"gef-remove-external-link gel-remove-external-link"}
            />
          </nav>
          : null
        }
      </Fragment>
    );

  }
}

export default SearchResults;

