import React, { Fragment } from 'react';
import SidebarDesktop from "../Sidebar/CatalogueDesktop";
import SearchResults from "../SearchResults/CatalogueSearchResults";
import LoadingIndicator from '../../components/LoadingIndicator';

const CatalogueSearchView = ({ loading, didYouMeanElement, isPrivate, focusInputOnSuggestionClick }) => (
  <Fragment>
    <article className="container pt-3">
      <div className="row">
        <aside className="col-12">
          <SidebarDesktop />
        </aside>
        <div className="col-12">
          {loading && <LoadingIndicator />}
          {!loading && <SearchResults />}
        </div>
      </div>
    </article>
  </Fragment>
);

export default CatalogueSearchView;