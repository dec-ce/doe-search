import React, { Fragment } from 'react';
import Sidebar from "../Sidebar";
import SearchResults from "../SearchResults/SiteSearchResults";
import SearchBox from '../SearchBox';
import LoadingIndicator from '../../components/LoadingIndicator';
import Badge from '../../components/Badge';

const SiteSearchView = ({ loading, didYouMeanElement, isPrivate, focusInputOnSuggestionClick }) => (
  <Fragment>
    <div className="container-fluid pl-0 pr-0">
      <div className="gel-search__banner">
        <div className="container">

          <div className="gel-search__banner__content">
            <div className="row mb-4">
              <div className="col">
                <h1 className="search__heading">
                  Search results
                  { isPrivate &&  <Badge icon="lock" label="Staff Only" /> }
                </h1>
              </div>
            </div>

            <div className="row mb-4">
              <div className="col">
                <SearchBox
                  focusInputOnSuggestionClick={focusInputOnSuggestionClick}
                  didYouMeanElement={didYouMeanElement}
                />
              </div>
            </div>

            {didYouMeanElement &&
              <div className="row">
                <div className="col">
                  {didYouMeanElement}
                </div>
              </div>
            }
          </div>

          <div className="row d-md-none">
            <div className="col">
              <Sidebar.Mobile />
            </div>
          </div>
        </div>
      </div>
    </div>

    <article className="container pt-3">
      <div className="row">
        <aside className="col-4 d-none d-md-block mt-5" aria-label="desktopResults">
          <Sidebar.Desktop />
        </aside>
        <div className="col-12 col-md-8">
          { loading && <LoadingIndicator /> }
          { !loading && <SearchResults /> }
        </div>
      </div>
    </article>
  </Fragment>
);

export default SiteSearchView;