import React  from 'react';
import LookElsewhere from '../../components/LookElsewhere';
import Facets from "../../components/Facets";
import ShowHide from "../../components/ShowHide";
import SearchContext from '../../shared/SearchContext';

class Sidebar extends React.PureComponent {
  static contextType = SearchContext.Context;

  render() {
    const { lookingForSomethingElse, query } = this.context;

    return (
      <ShowHide title="Refine Results">
        <div className="pt-2">
          <form action="" method="GET">
            <Facets location="mobile" />
          </form>
        </div>
        <hr className="gel-hr-light" />
        { lookingForSomethingElse &&
          <LookElsewhere.Sidebar
            location={
              lookingForSomethingElse.url &&
              `${lookingForSomethingElse.url}?q=${encodeURI(query || '')}`
            }
            text={lookingForSomethingElse.label}
            showLock={lookingForSomethingElse.showLock}
          />
        }
      </ShowHide>
    );
  }
}

export default Sidebar;
