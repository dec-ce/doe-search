import React, { Fragment } from 'react';
import SearchContext from '../../shared/SearchContext';

import LookElsewhere from '../../components/LookElsewhere';
import Facets from "../../components/Facets";
import BackTo from "../../components/BackTo";

class Sidebar extends React.PureComponent {
  static contextType = SearchContext.Context;

  render() {
    const { lookingForSomethingElse, backTo, query } = this.context;

    return (
      <Fragment>
        <h2 className="gel-typography__sidebar-heading mt-2 mb-3">
          Refine Results
        </h2>

        <hr className="gel-hr-search" />

        <form action="" method="GET">
          <Facets location="desktop"  />
        </form>

        { lookingForSomethingElse &&
          <Fragment>
            <hr className="gel-hr-search" />
            <LookElsewhere.Sidebar
              location={
                lookingForSomethingElse.url &&
                `${lookingForSomethingElse.url}?q=${encodeURI(query || '')}`
              }
              text={lookingForSomethingElse.label}
              showLock={lookingForSomethingElse.showLock}
            />
          </Fragment>
        }

        { backTo &&
          <Fragment>
            <hr className="gel-hr-search" />
            <BackTo
              url={backTo.url}
              label={backTo.label}
            />
          </Fragment>
        }

      </Fragment>
    );
  }
}

export default Sidebar;
