import { default as Desktop } from './Desktop';
import { default as Mobile } from './Mobile';

export default {
  Desktop,
  Mobile,
}
