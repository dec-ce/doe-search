import React, { Fragment } from 'react';
import SearchContext from '../../shared/SearchContext';
import Facets from "../../components/Facets";

class Sidebar extends React.PureComponent {
  static contextType = SearchContext.Context;

  onFilter = async (e) => {
    const { onSetPage, onSearchFetch } = this.context; 
    e.preventDefault();
    await onSetPage(0);
    await onSearchFetch();
  }

  render() {
    

    return (
      <Fragment>
        <form action="" method="GET">
          <div className="row">
            <Facets location="desktop" disableSearch displayColumn />
            <div className="mb-4 col-12 col-md-6 col-lg-4 ml-auto d-flex flex-column justify-content-end">
              <button
                onClick={this.onFilter}
                className="btn gel-button gel-button-primary gel-button-small w-100 mt-0 mb-0"
              >
                Filter
              </button>
            </div>
          </div>
        </form>
      </Fragment>
    );
  }
}

export default Sidebar;
