import React from 'react';
import { SidebarHeading } from '../components/Typography';

const MESSAGE_LOOK_ELSEWHERE = "looking for something else?"

const SidebarElement = ({ location, text, showLock }) => (
  <div className="looking-for-something-else-sidebar">
    <h3>
      { MESSAGE_LOOK_ELSEWHERE }
    </h3>
    <p>
      { showLock &&
        <span className="fa fa-lock pr-2" aria-hidden="true" />
      }
      <a href={location}>{ text }</a>
    </p>
  </div>
);

const MobileElement = ({ location, text, showLock }) => (
  <div className="gef-search-results__links">
    <SidebarHeading>
      { MESSAGE_LOOK_ELSEWHERE }
    </SidebarHeading>
    <p>
      { showLock &&
        <span className="fa fa-lock mr-2" aria-hidden="true" />
      }
      <a href={location}>{ text }</a>
    </p>
  </div>
);

const PageElement = ({ location, text, showLock, noResults }) => (
  <div className={`gel-panel ${noResults ? 'no-results' : ''}`}>
    <div className="looking-for-something-else">
      <h3>
        { MESSAGE_LOOK_ELSEWHERE }
      </h3>
      <p>
        { showLock &&
          <span className="fa fa-lock pr-2" aria-hidden="true" />
        }
        <a href={location}>{ text }</a>
      </p>
    </div>
  </div>
);

export default {
  Sidebar: SidebarElement,
  Page: PageElement,
  Mobile: MobileElement,
}
