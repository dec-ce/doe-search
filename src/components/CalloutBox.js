import React from 'react';

const CalloutBox = ({ title, children}) => (
  <div className="gel-call-out-box--lite">
    <h3 className="gel-call-out-box__heading">{ title }</h3>
    <div className="gel-call-out-box__content">
      { children }
    </div>
  </div>
);

export default CalloutBox;
