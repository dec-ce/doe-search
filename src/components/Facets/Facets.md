```
window.doeSearch = {
  isStaffOnly: true,
  lookingForSomethingElse: {
    showLock: true,
    label: 'Search staff only content',
    url: 'https://education.nsw.gov.au/inside-the-department/search',
  },
  facets: [
    {
      id: 'as_sitesearch',
      name: 'Search In',
      defaultValue: 'all',
      options: [
        { id: 'all', value: 'all', label: 'All of education.nsw.gov.au' },
        { id: 'atsp', value: '/atsp', label: 'Only ASTP'},
      ],
    },
    {
      id: 'filterBy',
      name: 'Type',
      label: 'Type',
      defaultValue: '',
      options: [
        { id: 'all', value: '', label: 'All' },
        { id: 'pages', value: 'pages', label: 'Pages' },
        { id: 'documents', value: 'documents', label: 'Documents'},
        { id: 'schools', value: 'schools', label: 'Schools'},
        { id: 'event', value: 'event', label: 'Events'},
      ],
    }
  ]
}

<Facets
  
/>
```