import React from 'react';

const FilterFacet = ({
  parentId,
  facetKey,
  facetName,
  options = {},
  onSelect,
  value: selectedValue,
  displayLabel,
}) => (
  <div>
    <fieldset className="form-group mb-0">
      <legend className={displayLabel ? 'form-label pl-0 pb-1' : 'sr-only'}>
        { displayLabel ? displayLabel : `Filter By ${ facetName }` }
      </legend>
      <div className="gel-select mb-0">
        <select onChange={(e) => onSelect(e.target.value)} value={selectedValue}>
          { options.map(({ id, value, label }) => (
            <option
              key={`${parentId ? `${parentId}_` : ''}filter_${facetKey}_${id}`}
              id={`${parentId ? `${parentId}_` : ''}filter_${facetKey}_${id}`}
              name={`filter_${facetKey}`}
              value={value}
            >
              { label }
            </option>
          )) }
        </select>
      </div>
    </fieldset>
  </div>
);

export default FilterFacet;