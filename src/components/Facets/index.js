import React, {Fragment} from 'react';
import FilterByRadio from './FilterByRadio';
import FilterBySelect from './FilterBySelect';
import SearchContext from '../../shared/SearchContext';

class Facets extends  React.Component {
  static contextType = SearchContext.Context;

  handleFacetChange = (facetKey) => async (value) => {
    const { disableSearch } = this.props;
    const { onSetFacet, onSetPage, onSearchFetch } = this.context;
    await onSetFacet(
      facetKey,
      value,
    );

    if (!disableSearch) {
      await onSetPage(0);
      await onSearchFetch();
    }
  };

  getSelected = (facetKey) => this.context.onGetFacet(facetKey);

  render() {
    const { doeSearch } = window;
    const { facets = [] } = doeSearch || {};
    const { displayColumn } = this.props;

    return (
      <Fragment>
        { facets.filter((facet) => !facet.hidden).map((facet) => (
          <div className={`mb-4 ${displayColumn ? 'col-12 col-md-6 col-lg-4' : ''}`} key={facet.id}>
            { facet.type === 'select' &&
              <FilterBySelect
                parentId={this.props.location}
                facetKey={facet.id}
                facetName={facet.name}
                options={facet.options || []}
                onSelect={this.handleFacetChange(facet.id)}
                value={this.getSelected(facet.id) || facet.defaultValue}
                displayLabel={facet.label}
              />
            }

            { facet.type !== 'select' &&
              <FilterByRadio
                parentId={this.props.location}
                facetKey={facet.id}
                facetName={facet.name}
                options={facet.options || []}
                onSelect={this.handleFacetChange(facet.id)}
                value={this.getSelected(facet.id) || facet.defaultValue}
                displayLabel={facet.label}
              />  
            }
          </div>
        )) }
      </Fragment>
    );
  }
}

export default Facets;
