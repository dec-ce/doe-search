import React from 'react';
import { Radio } from '../Input';

const FilterFacet = ({
  parentId,
  facetKey,
  facetName,
  options = {},
  onSelect,
  value: selectedValue,
  displayLabel,
}) => (
  <div>
    <fieldset className="form-group">
      <legend className={displayLabel ? 'form-label pl-0 pb-1' : 'sr-only'}>
        { displayLabel ? displayLabel : `Filter By ${ facetName }` }
      </legend>
      { options.map(({ id, value, label }) => (
        <Radio
          key={`${parentId ? `${parentId}_` : ''}filter_${facetKey}_${id}`}
          id={`${parentId ? `${parentId}_` : ''}filter_${facetKey}_${id}`}
          name={`filter_${facetKey}`}
          value={value}
          onChange={(e) => onSelect(e.target.value)}
          checked={value === selectedValue}
          label={label}
        />
      )) }
    </fieldset>
  </div>
);

export default FilterFacet;
