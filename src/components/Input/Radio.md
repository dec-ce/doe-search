```
<fieldset className="form-group">
  <legend className="form-label pl-0 pb-1">Radios</legend>
  <div>
    <Radio
      name="exampleRadios"
      id="exampleRadios1"
      value="option1"
      label="All of education.nsw.gov.au"
      checked
    />
    <Radio
      name="exampleRadios"
      id="exampleRadios2"
      value="option2"
      label="Learning Management Business Reform"
    />
    <Radio
      name="exampleRadios"
      id="exampleRadios3"
      value="option3"
      label="DEC International"
      disabled
    />
  </div>
</fieldset>
```


| Property | Type | Description |
| --- |
| name | string | Input HTML name attribute is used to identify a group of radio buttons |
| id | string | ID tag of the HTML element |
| value | string | |
| label | string | |
| checked | boolean | |
| disabled | boolean | |
