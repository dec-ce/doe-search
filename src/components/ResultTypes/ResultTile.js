import React from "react";

const ResultTile = ({ title, url, body, image = {} }) => (
  <a className="gel-featured-teaser-link d-flex w-100 gel-remove-external-link" href={url}>
    <div
      className="card gel-featured-teaser_variant d-flex w-100"
      data-pseudo-link="data-pseudo-link"
    >
      {image.src && (
        <img className="img-fluid card-img-top" src={image.src} alt={image.alt} />
      )}
      <div className="card-body gel-featured-teaser__content_variant">
        <h4 className="card-title" style={{ maxHeight: 'none' }}>{title}</h4>
        <p dangerouslySetInnerHTML={{ __html: body }}></p>
        <div className="gel-featured-teaser__arrow">
          <span className="far fa-long-arrow-right"></span>
        </div>
      </div>
    </div>
  </a>
);

export default ResultTile;
