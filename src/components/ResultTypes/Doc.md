```
<Document
  resultType="pdf"
  title="PDF Document"
  url="http://education.nsw.gov.au"
  summary="Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Cras justo odio, dapibus ac facilisis in, egestas."
/>
```

```
<Document
  resultType="doc"
  title="Word Document"
  url="http://education.nsw.gov.au"
  summary="Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Cras justo odio, dapibus ac facilisis in, egestas."
/>
```

```
<Document
  resultType="xls"
  title="Excel Document / Spreadsheet"
  url="http://education.nsw.gov.au"
  summary="Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Cras justo odio, dapibus ac facilisis in, egestas."
/>
```

```
<Document
  resultType="ppt"
  title="PowerPoint Document"
  url="http://education.nsw.gov.au"
  summary="Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Cras justo odio, dapibus ac facilisis in, egestas."
/>
```

```
<Document
  resultType="policy"
  title="Policy Document"
  url="http://education.nsw.gov.au"
  summary="Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Cras justo odio, dapibus ac facilisis in, egestas."
/>
```

```
<Document
  resultType="file"
  title="File"
  url="http://education.nsw.gov.au"
  summary="Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Cras justo odio, dapibus ac facilisis in, egestas."
/>
```

```
<Document
  resultType="file"
  title="File"
  url="http://education.nsw.gov.au"
  summary="Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Cras justo odio, dapibus ac facilisis in, egestas."
  isStaffOnly
/>
```