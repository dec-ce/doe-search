```
<Calendar
  eventDate="8"
  eventMonth="July"
  eventYear="2019"
  eventWeekday="Monday"

  eventUrl="http://education.nsw.gov.au"
  eventTitle="Placeholder Title"
  eventDesciption={`<span>Placeholder description <strong>here</strong></span>`}
  
  eventTimeString="9:30am - 3:00pm"
  eventLocationString="105 Phillip Street, Parramatta"
/>
```