import React from 'react';
import Card from '../Card';
import StaffOnlyItem from './StaffOnly';

const HtmlFragment = ({ result, summaryHtml, isStaffOnly }) => (
  <Card type="web">
    <h2 className="gel-typography__link-heading">
      <a
        className="mr-6"
        href={result.url}
        dangerouslySetInnerHTML={{ __html: result.title }}
      />
      { isStaffOnly
        ? <StaffOnlyItem />
        : null
      }
    </h2>
    <p dangerouslySetInnerHTML={{ __html: summaryHtml }} />
    <p
      className="gel-search-results__url"
      dangerouslySetInnerHTML={{ __html: result.url }}
    />
  </Card>
);

export default HtmlFragment;
