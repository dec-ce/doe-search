import React from 'react';
import Badge from '../Badge';

const StaffOnly = props => (
  <Badge icon="lock" label="STAFF ONLY" {...props} />
);

export default StaffOnly;