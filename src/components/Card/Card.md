#### Card with Label
```
<Card label="Features">
  The quick brown fox jumps over the lazy dog
</Card>
```

#### Card without Label
```
<Card>
  The quick brown fox jumps over the lazy dog
</Card>
```
