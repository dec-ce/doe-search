import React from 'react';

const LoadingIndicator = () => (
  <div className="gel-loader" />
);

export default LoadingIndicator;