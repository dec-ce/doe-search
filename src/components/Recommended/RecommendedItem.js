import React from 'react';

const RecommendedItem = (props) => {
  return (
    <li className="gel-search__item">
      <h2 className="gel-typography__link-heading">
        <a href={props.url}>{props.title}</a>
      </h2>
      <p>{ props.content }</p>
      <p className="gel-search-results__url">{props.url}</p>
    </li>
  );
};

export default RecommendedItem;