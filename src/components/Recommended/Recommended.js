import React from 'react';
import CalloutBox from "../CalloutBox";

const Recommended = ({ children }) => {
  return (
    <CalloutBox title="Recommended">
      <ol className="gel-search-results__list mt-0 mb-0">
        { children }
      </ol>
    </CalloutBox>
  );
};

export default Recommended;