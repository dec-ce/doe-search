import React from 'react';

const BackTo = ({ label, url }) => {
  return (
    <div className="gel-search-results__links">
      <a
        href={url}
        className="btn gel-button gel-button-primary gel-button-l-icon gel-button-small"
      >
        Back
        <span className="sr-only">to { label }</span>
      </a>
    </div>
  );
}

export default BackTo;