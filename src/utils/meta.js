const getSiteOptions = () => {
    const { doeSearch = {} } = window;
    const { endpoint, siteUrl, collection, ignoreSiteForGlobal } = doeSearch;
    
    return {
        baseUrl: endpoint || process.env.BASE_SEARCH_URL,
        collection: collection || document.head.getAttribute("data-site-collection") || process.env.DEFAULT_COLLECTION || 'public_education_nsw_gov_au',
        site: siteUrl || document.head.getAttribute("data-site-url") || process.env.DEFAULT_SITE || '',
        client: document.head.getAttribute("data-site-client") || process.env.DEFAULT_CLIENT || 'doe',
        ignoreSiteForGlobal: ignoreSiteForGlobal || !!document.head.getAttribute("data-ignore-site-for-global"),
    };
};

export {getSiteOptions};