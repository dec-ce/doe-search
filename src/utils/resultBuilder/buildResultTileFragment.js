import React from "react";
import ResultTile from "../../components/ResultTypes/ResultTile";

const buildMapFragment = (result = {}) => {
  const { metatags = {} } = result;

  const image =
    metatags["og:image:secure_url"] ||
    metatags["twitter:image"] ||
    "";

  return (
    <div className="col-12 col-md-6 col-lg-4 mb-4 d-flex" key={result.id}>
      <ResultTile
        title={result.title}
        url={result.url}
        body={metatags["description"] || metatags["Description"]}
        image={!!image && { src: image, alt: "" }}
      />
    </div>
  );
};

export default buildMapFragment;
