import React from 'react';
import Search from './containers/Search';
import SearchContext from './shared/SearchContext.js';

const app = (props) => (
  <SearchContext.Provider>
    <Search { ...props } />
  </SearchContext.Provider>
);

export default app;
