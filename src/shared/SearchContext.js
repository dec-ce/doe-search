import React from 'react';
import queryString from 'query-string';

import { escapeRegexCharacters, debounce } from '../utils/utils';
import { getSiteOptions } from '../utils/meta';
import { search, searchSuggestions } from '../services/search';
const SearchContext = React.createContext();

function parseHtmlContent(spelling) {
  let tempDiv = document.createElement("div");
  tempDiv.innerHTML = spelling;
  return tempDiv.textContent || tempDiv.innerText || "";
}

const itemsPerPage = 10;

function getItemsPerPage() {
  const { doeSearch } = window;
  const { view } = doeSearch || {};
  const { type } = view || {};

  if (type === 'catalogue') {
    return 12;
  }

  return itemsPerPage;
}

const defaultState = {
  query: '',
  lastSearchQuery: '',

  facets: {},

  loading: false,

  selectedPage: 0,
  start: 0,
  suggestions: [],

  result: undefined,
  didYouMean: undefined,
  didYouMeanHtml: undefined,

  isSecure: false,
  searchInText: 'NSW Department of Education',
  lookingForSomethingElse: undefined,
};

function getHiddenFacets() {
  const { doeSearch } = window;
  const { facets: facetConfig = [] } = doeSearch || {};

  const hiddenFacets = facetConfig
    .filter((facet) => facet.hidden === true || facet.hidden === 'true')
    .reduce((acc, val) => {
      if (!val.id) {
        return { ...acc }
      }

      return {
        ...acc,
        [val.id]: val.value || val.defaultValue || '',
      }
    }, {});

  return hiddenFacets || {};
}

class SearchProvider extends React.Component {
  state = {
    ...defaultState,
  };

  // TODO: only call update if value !== stored value
  onSetQuery = (query, cb) => this.setState({ query }, cb);
  onSetStart = (start = 0, cb) => this.setState({ start }, cb);
  onSetPage = (page = 0, cb) => this.setState({
    selectedPage: page,
    start: page * getItemsPerPage(),
  }, cb);
  
  onSetSearchInText = (value, cb) => this.setState({
    searchInText: value,
  }, cb);

  onSetLookingForSomethingElse = (value, cb) => this.setState({
    lookingForSomethingElse: value,
  }, cb);

  onSetBackTo = (value, cb) => this.setState({
    backTo: value,
  }, cb);

  onSetIsSecure = (value, cb) => this.setState({
    isSecure: value,
  }, cb);

  onUpdateQueryParams = (options = {}) => {
    const { location } = window;
    const { origin, pathname, search } = location || {};

    const { query } = this.state;

    const queryParams = queryString.parse(search);
    const updatedParams = Object.keys(options).reduce((acc, value) => {
      return { ...acc, [value]: options[value] }
    }, queryParams);

    updatedParams.q = query;

    const updatedQueryString = queryString.stringify(updatedParams);
    const updatedLocation = `${origin}${pathname}?${updatedQueryString}`;

    const isChanged = (
      JSON.stringify(queryString.parse(search)) !== JSON.stringify(updatedParams)
    );

    if (window.history.replaceState && isChanged) {
      window.history.replaceState({}, '', updatedLocation);
    }

    if (window.dataLayer && isChanged) {
      const { baseUrl, collection, site, client, ignoreSiteForGlobal } = getSiteOptions() || {};
      const { q: query, filterBy, as_sitesearch, ...otherParams } = updatedParams;
      window.dataLayer.push({
        'event' : 'searchRefine',
        query,
        filterBy: !!filterBy ? filterBy : 'all',
        as_sitesearch: !!as_sitesearch ? as_sitesearch : 'all',

        baseUrl,
        collection,
        site,
        client,
        ignoreSiteForGlobal,

        ...otherParams,

        path: `${pathname}?${updatedQueryString}`
      });
    }
  };

  onSetFacet = (facetKey, facetValue, cb) => {
    this.onUpdateQueryParams({ [facetKey]: facetValue });
    return this.setState({
      facets: {
        ...this.state.facets,
        [facetKey]: facetValue,
      }
    }, cb);
  };

  onSetFacets = (facets, cb) => {
    return this.setState({
      facets: {
        ...this.state.facets,
        ...facets,
      }
    }, cb);
  };

  onGetFacet = (facetKey) => this.state.facets[facetKey];
  onSearchFetch = (updateQuery, callback) => {
    const { query, lastSearchQuery, start, facets} = this.state;
    const { as_sitesearch, filterBy, ...otherFacets } = facets;

    const searchQuery =  typeof query !== 'undefined' ? query : lastSearchQuery;

    if (updateQuery) {
      this.onUpdateQueryParams({ q: searchQuery });
    }

    this.setState({ loading: true });

    search(searchQuery, as_sitesearch, filterBy, start, { ...otherFacets, ...getHiddenFacets() })
      .then((response) => {
        if (response) {
          this.setState({
            lastSearchQuery: searchQuery,
            result: response,
            didYouMean: response.didYouMean ? response.didYouMean["suggest"] : '',
            didYouMeanHtml: response.didYouMean ? parseHtmlContent(response.didYouMean["highlightedSuggest"]) : '',
            loading: false,
          }, callback);
        }
      })
      .catch((error) => {
        this.setState({
          error,
          loading: false,
        }, callback);
      });
  };

  onResetSearch = () => {
    this.setState({ ...defaultState });
  };

  onSuggestionsFetch = debounce(() => {
    const { query = '', facets } = this.state;
    const { as_sitesearch, filterBy } = facets;

    const escapedValue = escapeRegexCharacters(query.trim());

    if (escapedValue === '') {
      return this.setState({ suggestions: [] });
    }

    searchSuggestions(escapedValue, as_sitesearch, filterBy)
      .then((response) => {
        this.setState({ suggestions: response || [] });
      })
      .catch((error) => {
        this.setState({error});
        console.error(error)
      });
  }, 300);

  onSuggestionsClear = () => this.setState({ suggestions: [] });

  render() {
    const {
      query,
      lastSearchQuery,
      suggestions,
      result,
      selectedPage,
      facets,
      didYouMean,
      didYouMeanHtml,
      loading,
      isSecure,
      searchInText,
      lookingForSomethingElse,
      backTo
    } = this.state;

    const { children } = this.props;

    return (
      <SearchContext.Provider
        value={{
          query,
          lastSearchQuery,

          suggestions,
          result,
          selectedPage,
          facets,

          didYouMean,
          didYouMeanHtml,

          loading,

          isSecure,
          searchInText,
          lookingForSomethingElse,

          onSetQuery: this.onSetQuery,
          onSetStart: this.onSetStart,
          onSetPage: this.onSetPage,
          onSetFacet: this.onSetFacet,
          onSearchFetch: this.onSearchFetch,
          onSuggestionsFetch: this.onSuggestionsFetch,
          onSuggestionsClear: this.onSuggestionsClear,
          onResetSearch: this.onResetSearch,
          onGetFacet: this.onGetFacet,
          onSetFacets: this.onSetFacets,

          onSetSearchInText: this.onSetSearchInText,
          onSetLookingForSomethingElse: this.onSetLookingForSomethingElse,
          onSetIsSecure: this.onSetIsSecure,

          onSetBackTo: this.onSetBackTo,
          backTo,
        }}
      >
        { children }
      </SearchContext.Provider>
    )
  }
}

export default {
  Provider: SearchProvider,
  Consumer: SearchContext.Consumer,
  Context: SearchContext,
}
