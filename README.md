<div align="center">
  <a href="https://education.nsw.gov.au">
    <img alt="NSW Government" src="https://t3.gstatic.com/images?q=tbn:ANd9GcRl0UoerogYRIjSXZj5n2cxfIMVA2puwEeVCFWRRe5vPBNoFl_b" height="150px" />
  </a>
</div>

<br />

<div align="center">
  <strong>
    NSW Department of Education
  </strong>
  <br />
  Search Single Page Application (SPA) - built with React
</div>

<br />
<br/>

This Single Page Application (SPA) is built using [React](https://reactjs.org/) and provides a responsive search experience intended to power the NSW Department of Education corporate and schools websites.

# Develop
In order to run and develop this application you must have `node` (v8 or greater) installed along with `yarn`.

## Getting Started
In order to get started you will need to install all the dependant packages; and then run the application. This can be done as follows.
```
$ yarn              // or `yarn install`
$ yarn start        // calls the start script defined in package.json
```
> *Note:* <br />
> On a *nix (linux, unix, mac) based OS you can call these commands in one line by using a pipe `&&` or on Windows `||` <br/>
> &nbsp;&nbsp; I.e. `yarn && yarn run` <br />

## Local Development
When using search with other projects you may wish to circumnavigate having to commit changes to search then update the reference.

Built into yarn there is a feature to make this process a bit easier. <br/>
By running the following inside the doe-search project you can initiate a link.
```
$ yarn link
```

Then inside the project that is referencing the `doe-search` project you can run the following
```
$ yarn link doe-search 
```

Behind the scenes this will create a sym-link between the doe-search folder and the package folder within the `node_modules` folder. <br/>
However you may need to run a build (`yarn build`) inside the doe-search project.

## Theme Development
Being this project is based around the `GEL` library, inherenty there isn't any major colors and styles within the search. As a result you will need to theme components used within search within the themed implementation of the GEL library.


# Build

## Running a manual build
The compiled/built version of search is located in the `/dist` folder. in order to build the Search SPA it's a simple as calling.
```
$ yarn build
```

> *Note:* <br/>
> You don't need to commit the build for a release; as when you merge to master it will do a build and version.



## Automated build pipelines
Bitbucket pipelines has been setup in such a way that it monitors commits to master. Once a merge or commit to master is made it will attempt to trigger a pipeline.

> *Note:* <br/>
> If you want to commit to master without triggering the pipeline then you can include `[skip-ci]` in the commit message and it will skip the pipeline that builds, versions, and tags.

Currently (at writing) when the pipeline runs it will:
1. Initialise the container and installing dependencies
2. Increment the `package.json` version semver by one (1) minor
3. Perform a build
4. Commit any changes (most noteably `/dist` and `yarn.lock`)
5. Push commit
6. Tag commit


```yml
# bitbucket-pipelines.yml

image: node:8.16.0

pipelines:
  branches:
    master:
      - step:
          caches:
            - node
          script: 
            # Install JQ package
            - apt-get update
            - apt-get install jq -y

            # Install Dependencies
            - yarn install
            
            # Create a new version of search
            - yarn gulp autoversion
            
            # Run a new build
            - yarn build
            
            # Create a new commit and tag with new version
            - git config remote.origin.url git@bitbucket.org:dec-ce/doe-search.git
            - git init
            - git config user.name "Digital Experience Design (DxD)"
            - git config user.email "communication@det.nsw.edu.au"
            
            - git add .
            - git commit -m "Updated build of doe-search" -m "[skip ci]"
            - git push
            
            - declare -x VERSION=$(jq -r '.version' package.json)
            - echo $VERSION
            - git tag $VERSION
            - git remote -v
            - git push origin --tags
```


# Config
There are various ways to provide configuration for the search, the two key methods are:
- HTML data tags
- JSON object on window (`doeSearch`)

> *Note:* <br />
> Properties in the JSON object in most cases get precedent over the HTML data tags.

| JSON Variable | HTML Data Tag | Type | Description |
| - | - | - | - |
| - | data-site-collection | `string` | Collection of search indexes |
| siteUrl | data-site-url | `string` | URL that is used for `as_sitesearch` property (without *htt(s)://*) |
| - | data-site-client | `string` |  |
| ignoreSiteForGlobal | data-ignore-site-for-global | `boolean` *OR* `string` | Doesn't pass throught `site url` to `as_sitesearch` when there is no filterBy |
| searchingIn | - | `string` | The text that is displayed above results. <br> <i>x results for **query** in **searchIn** </i>|
| isStaffOnly | - | `boolean` | Displays the lock badge next to the **Search results** text |
| - | - | - | - |
| lookingForSomethingElse | - | `object` | Configuration for the looking for something else elements |
| lookingForSomethingElse.showLock | - | `boolean` |  |
| lookingForSomethingElse.label | - | `string` |  |
| lookingForSomethingElse.url | - | `string` |  |
| lookingForSomethingElse.url | - | `string` |  |
| - | - | - | - |
| facets | - | `array` |  |
| facets[0].id | - | `string` |  |
| facets[0].name | - | `string` |  |
| facets[0].defaultValue | - | `string` |  |
| facets[0].hidden | - | `string` | Hides the search facet |
| facets[0].type | - | `string` | `select` or `radio` |
| facets[0].value | - | `string` | Used when hidden is set |
| facets[0].options | - | `array` |  |
| facets[0].options[0].id | - | `string` |  |
| facets[0].options[0].value | - | `string` |  |
| facets[0].options[0].label | - | `string` |  |
| view | - | `object` | Settings that control how the search UI will be rendered. |
| view.type | - | `string` | Setting that determined the search UI that will be rendered. <br/> "catalogue" =  Renders a catalogue view for search.<br/> default = Renders the default view if view type is empy or '' or 'site'.



## Sample JSON configuration
```html
<script>
  window.doeSearch = {}
</script>
```

```json
{
  "searchingIn": "NSW Department of Education",
  "isStaffOnly": false,
  "lookingForSomethingElse": {
    "showLock": true,
    "label": "Search staff only content",
    "url": "https://education.nsw.gov.au/inside-the-department/search",
  },
  "facets": [
    {
      "id": "as_sitesearch",
      "name": "Search In",
      "defaultValue": "all",
      "options": [
        { "id": "all", "value": "all", "label": "All of education.nsw.gov.au" },
        { "id": "atsp", "value": "/atsp", "label": "Only ASTP"},
        { "id": "policy", "value": "/policy-library", "label": "Policy Library"},
      ],
    },
    {
      "id": "filterBy",
      "name": "Type",
      "label": "Type",
      "defaultValue": "",
      "options": [
        { "id": "all", "value": "", "label": "All" },
        { "id": "pages", "value": "pages", "label": "Pages" },
        { "id": "documents", "value": "documents", "label": "Documents"},
        { "id": "schools", "value": "schools", "label": "Schools"},
        { "id": "event", "value": "event", "label": "Events"},
        { "id": "policy", "value": "policy", "label": "Policies" },
        { "id": "article", "value": "article", "label": "Article" }
      ],
    }
  ],
  "siteUrl": "education.nsw.gov.au",
  "ignoreSiteForGlobal": true,
}
```
