const gulp = require('gulp');
const runSequence = require('run-sequence'); // Run tasks sequentially
const jsonModify = require('gulp-json-modify');

gulp.task('upversion', function () {
  let ver = require('./package.json').version; //version defined in the package.json file

  console.log('current version: ', ver);
  let splitString = ver.split('.', 3);
  let patchVersion = splitString[2].split('"',1);
  let patchNumber = Number(patchVersion[0]);

  patchNumber += 1;
  splitString[2] = String(patchNumber);
  process.env.VERSION = splitString.join('.');
  console.log(process.env.VERSION);
})

gulp.task('saveversion', function () {
  return gulp.src(['./package.json'])
    .pipe(jsonModify({
      key: 'version',
      value: process.env.VERSION
    }))
    .pipe(gulp.dest('./'))
})

// Autoversion Script Source:
// https://medium.com/@shanecfast/automate-versioning-using-bitbucket-pipelines-nodejs-220d2a4c3168
gulp.task('autoversion', function () {
  runSequence('upversion','saveversion');
})